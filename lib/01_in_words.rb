require 'byebug'

class Fixnum
  @@word_bank = Hash[0, "zero", 1, "one", 2, "two", 3, "three", 4, "four", 5, "five", 6, "six", 7, "seven", 8, "eight", 9, "nine", 10, "ten", 11, "eleven", 12, "twelve", 13, "thirteen", 14, "fourteen", 15, "fifteen", 16, "sixteen", 17, "seventeen", 18, "eighteen", 19, "nineteen", 20, "twenty", 30, "thirty", 40, "forty", 50, "fifty", 60, "sixty", 70, "seventy", 80, "eighty", 90, "ninety", 100, "one hundred", 1000, "one thousand", 1_000_000, "one million", 1_000_000_000, "one billion", 1_000_000_000_000, "one trillion"]



  def in_words
    return @@word_bank[self] if @@word_bank.include?(self)

    count_digits(self)
  end

  private

  def count_digits(num)
    return nil if num == 0
    return @@word_bank[num] if @@word_bank.include?(num)
    case num.to_s.length
    when 1..2
      double(num)
    when 3
      hundreds(num)
    when (4..6)
      thousands(num)
    when 7..9
      millions(num)
    when 10..12
      billions(num)
    else
      trillions(num)
    end
  end

  def double(num)
    num = num.to_s
    first_digit = num[-1].to_i
    second_digit = num.to_i - first_digit
    @@word_bank[second_digit] + " " + @@word_bank[first_digit]
  end

  def hundreds(num)
    translate_to_word("hundred", -2, num)
  end


  def thousands(num)
    translate_to_word("thousand", -3, num)
  end

  def millions(num)
    translate_to_word("million", -6, num)
  end

  def billions(num)
    translate_to_word("billion", -9, num)
  end

  def trillions(num)
    translate_to_word("trillion", -12, num)
  end

  def translate_to_word(placement, range, num)
    [count_digits(num.to_s[0..range-1].to_i),
    placement,
    count_digits(num.to_s[range..-1].to_i)].compact.join(" ")
  end



end
